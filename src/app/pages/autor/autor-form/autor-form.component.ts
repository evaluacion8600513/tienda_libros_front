import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { AutorService } from 'src/app/services/autor/autor.service';
declare var $: any;

@Component({
  selector: 'app-autor-form',
  templateUrl: './autor-form.component.html',
  styleUrls: ['./autor-form.component.css'],
})
export class AutorFormComponent {
  @Input() tituloForm!: string;
  @Input() isEdit!: boolean;
  @Input() item: any;
  @Output() clearItem = new EventEmitter<boolean>();
  isClicked: boolean = false;
  router: any;
  autores: any;

  constructor(
    public autorService: AutorService,
    private alert: AlertsService
  ) {}

  ngOnInit() {}

  ngOnChanges() {
    this.recoveryAutor();
  }

  get nombre() {
    return this.formAutor.get('nombre') as FormControl;
  }

  get paterno() {
    return this.formAutor.get('paterno') as FormControl;
  }

  get materno() {
    return this.formAutor.get('materno') as FormControl;
  }

  get sexo() {
    return this.formAutor.get('sexo') as FormControl;
  }

  get direccion() {
    return this.formAutor.get('direccion') as FormControl;
  }

  get telefono() {
    return this.formAutor.get('telefono') as FormControl;
  }

  get email() {
    return this.formAutor.get('email') as FormControl;
  }

  formAutor = new FormGroup({
    nombre: new FormControl('', Validators.required),
    paterno: new FormControl('', Validators.required),
    materno: new FormControl('', Validators.required),
    sexo: new FormControl('', Validators.required),
    direccion: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  createAutor() {
    this.isClicked = true;
    if (this.formAutor.valid) {
      if (this.isEdit) {
        this.autorService
          .putUpdateAutor(this.item.id, this.formAutor.value)
          .subscribe({
            next: (val: any) => {
              if (val.data) {
                this.alert.sweet(
                  'Edición Autor',
                  'Se realizó correctamente.',
                  'success'
                );
                this.limpiar();
                this.autorService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              } else {
                this.alert.sweet(
                  'Edición Autor',
                  'No se pudo realizar la edición.',
                  'error'
                );
                this.limpiar();
                this.autorService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              }
            },
          });
      } else {
        this.autorService.postAddAutor(this.formAutor.value).subscribe({
          next: (val: any) => {
            if (val.estado) {
              this.alert.sweet(
                'Registro Autor',
                'Se realizó correctamente.',
                'success'
              );
              this.limpiar();
              this.autorService.refresh = true;
            } else {
              this.alert.sweet(
                'Registro de Autor',
                'No se pudo realizar el registro. ',
                'error'
              );
              this.limpiar();
              this.autorService.refresh = true;
            }
          },
        });
        this.isClicked = false;
        $('#modal-add-edit').modal('hide');
      }
    }
  }

  recoveryAutor() {
    if (this.item) {
      this.autorService.getByIDAutor(this.item.id).subscribe({
        next: (val: any) => {
          this.formAutor.patchValue(val.data);
        },
      });
    }
  }

  limpiar() {
    this.formAutor.reset();
    this.item = '';
    this.isClicked = false;
    this.clearItem.emit(true);
  }
}
