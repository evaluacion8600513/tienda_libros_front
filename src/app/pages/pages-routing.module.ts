import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { HomeComponent } from '../shared/components/home/home.component';
import { NgModule } from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: '', component: HomeComponent },
      {
        path: 'libro',
        loadChildren: () =>
          import('./libro/libro.module').then((m) => m.LibroModule),
      },
      {
        path: 'autor',
        loadChildren: () =>
          import('./autor/autor.module').then((m) => m.AutorModule),
      },
      {
        path: 'editorial',
        loadChildren: () =>
          import('./editorial/editorial.module').then((m) => m.EditorialModule),
      },
      {
        path: 'categoria',
        loadChildren: () =>
          import('./categoria/categoria.module').then((m) => m.CategoriaModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
