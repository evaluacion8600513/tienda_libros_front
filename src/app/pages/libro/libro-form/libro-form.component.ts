import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { LibroService } from 'src/app/services/libro/libro.service';
import { AutorService } from 'src/app/services/autor/autor.service';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import { EditorialService } from 'src/app/services/editorial/editorial.service';
declare var $: any;

@Component({
  selector: 'app-libro-form',
  templateUrl: './libro-form.component.html',
  styleUrls: ['./libro-form.component.css'],
})
export class LibroFormComponent {
  @Input() tituloForm!: string;
  @Input() isEdit!: boolean;
  @Input() item: any;
  @Output() clearItem = new EventEmitter<boolean>();
  isClicked: boolean = false;
  router: any;
  listaAutores: any;
  listaCategorias: any;
  listaEditoriales: any;
  selectedLibro!: number;
  selectAutores!: number;
  selectEditoriales!: number;
  selectCategorias!: number;

  constructor(
    public libroService: LibroService,
    public categoriaService: CategoriaService,
    public EditorialService: EditorialService,
    public AutorService: AutorService,
    private alert: AlertsService
  ) {}

  ngOnInit() {
    this.comboAutor();
    this.comboCategorias();
    this.comboEditoriales();
  }

  ngOnChanges() {
    this.recoveryLibro();
  }

  get titulo() {
    return this.formLibro.get('titulo') as FormControl;
  }

  get num_edicion() {
    return this.formLibro.get('num_edicion') as FormControl;
  }

  get num_paginas() {
    return this.formLibro.get('num_paginas') as FormControl;
  }

  get id_categorias() {
    return this.formLibro.get('id_categorias') as FormControl;
  }

  get fecha_publicacion() {
    return this.formLibro.get('fecha_publicacion') as FormControl;
  }
  get sinopsis() {
    return this.formLibro.get('sinopsis') as FormControl;
  }

  get img() {
    return this.formLibro.get('img') as FormControl;
  }

  get cantidad() {
    return this.formLibro.get('cantidad') as FormControl;
  }

  get id_editoriales() {
    return this.formLibro.get('id_editoriales') as FormControl;
  }

  get autores() {
    return this.formLibro.get('autores') as FormControl;
  }

  formLibro = new FormGroup({
    titulo: new FormControl('', Validators.required),
    num_edicion: new FormControl('', Validators.required),
    num_paginas: new FormControl('', Validators.required),
    id_categorias: new FormControl('', Validators.required),
    fecha_publicacion: new FormControl('', Validators.required),
    sinopsis: new FormControl('', Validators.required),
    img: new FormControl('', Validators.required),
    cantidad: new FormControl('', Validators.required),
    id_editoriales: new FormControl('', Validators.required),
    autores: new FormControl('', Validators.required),
  });

  createLibro() {
    this.isClicked = true;
    if (this.formLibro.valid) {
      if (this.isEdit) {
        console.log(this.item);
        this.libroService
          .putUpdateLibro(
            this.item.libros[0].id,
            this.item.id,
            this.formLibro.value
          )
          .subscribe({
            next: (val: any) => {
              if (val.data) {
                this.alert.sweet(
                  'Edición Libro',
                  'Se realizó correctamente.',
                  'success'
                );
                this.limpiar();
                this.libroService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              } else {
                this.alert.sweet(
                  'Edición Cliente',
                  'No se pudo realizar la edición.',
                  'error'
                );
                this.limpiar();
                this.libroService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              }
            },
          });
      } else {
        this.libroService.postAddLibro(this.formLibro.value).subscribe({
          next: (val: any) => {
            if (val.estado) {
              this.alert.sweet(
                'Registro Libro',
                'Se realizó correctamente.',
                'success'
              );
              this.limpiar();
              this.libroService.refresh = true;
            } else {
              this.alert.sweet(
                'Registro de Unidad',
                'No se pudo realizar el registro. ',
                'error'
              );
              this.limpiar();
              this.libroService.refresh = true;
            }
          },
        });
        this.isClicked = false;
        $('#modal-add-edit').modal('hide');
      }
    }
  }

  recoveryLibro() {
    if (this.item) {
      this.libroService.getByIDLibro(this.item.id).subscribe({
        next: (val: any) => {
          this.formLibro.patchValue(val.libro);
          this.selectAutores = val.autores.id;
        },
      });
    }
  }

  limpiar() {
    this.formLibro.reset();
    this.item = '';
    this.isClicked = false;
    this.clearItem.emit(true);
  }

  comboCategorias() {
    this.categoriaService.getComboCategoria().subscribe({
      next: (val: any) => {
        this.listaCategorias = val;
      },
    });
  }

  comboEditoriales() {
    this.EditorialService.getComboEditorial().subscribe({
      next: (val: any) => {
        this.listaEditoriales = val;
      },
    });
  }

  comboAutor() {
    this.AutorService.getComboAutor().subscribe({
      next: (val: any) => {
        this.listaAutores = val;
      },
    });
  }
}
