import { VariableBinding } from '@angular/compiler';
import { Component, Input } from '@angular/core';
import { LibroService } from 'src/app/services/libro/libro.service';

@Component({
  selector: 'app-libro-detalles',
  templateUrl: './libro-detalles.component.html',
  styleUrls: ['./libro-detalles.component.css'],
})
export class LibroDetallesComponent {
  @Input() item: any;
  libro: any;
  autor: any;

  constructor(public libroService: LibroService) {}

  ngOnChanges() {
    this.recoveryAutor();
  }

  recoveryAutor() {
    if (this.item) {
      this.libroService.getByIDLibro(this.item.id).subscribe({
        next: (val: any) => {
          this.libro = val.libro[0];
          console.log(val);
          this.autor = val.autores;
          console.log(this.autor);
        },
      });
    }
  }
}
