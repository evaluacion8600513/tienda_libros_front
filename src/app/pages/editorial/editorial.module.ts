import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditorialComponent } from './editorial.component';
import { EditorialListComponent } from './editorial-list/editorial-list.component';
import { EditorialFormComponent } from './editorial-form/editorial-form.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import { EditorialRoutingModule } from './editorial-routing.module';

@NgModule({
  declarations: [
    EditorialComponent,
    EditorialListComponent,
    EditorialFormComponent,
  ],
  imports: [
    SharedModule,
    CommonModule,
    EditorialRoutingModule,
    NgxPaginationModule,
    FormsModule,
    ReactiveFormsModule,
    NgSelectModule,
  ],
})
export class EditorialModule {}
