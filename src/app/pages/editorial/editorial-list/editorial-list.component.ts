import { Component } from '@angular/core';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { EditorialService } from 'src/app/services/editorial/editorial.service';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-editorial-list',
  templateUrl: './editorial-list.component.html',
  styleUrls: ['./editorial-list.component.css'],
})
export class EditorialListComponent {
  buscar = { editorial: '' };
  cargando = false;
  editorial!: any[];
  pagination: number = 1;
  cantReg: number = 10;
  allEditorial: number = 0;
  public hasError: boolean = false;

  tituloForm = '';
  isEdit: boolean = false;
  item: any;

  constructor(
    public editorialService: EditorialService,
    public alert: AlertsService
  ) {}

  ngOnInit() {
    this.listEditorial();
  }

  ngDoCheck() {
    if (this.editorialService.refresh) {
      this.numRegistro();
      this.editorialService.refresh = false;
    }
  }

  listEditorial() {
    this.editorialService
      .getEditorial(this.pagination, this.cantReg)
      .subscribe({
        next: (res: any) => {
          this.editorial = res.data;
          this.allEditorial = res.total;
          this.cargando = true;
        },
      });
  }

  findEditorial() {
    this.cargando = false;
    if (this.buscar.editorial) {
      this.editorialService
        .getLikeEditorial(this.pagination, this.cantReg, this.buscar.editorial)
        .subscribe({
          next: (res: any) => {
            this.editorial = res.data;
            this.allEditorial = res.total;
            this.pagination = res.current_page;
            this.cargando = true;
          },
        });
    } else {
      this.listEditorial();
    }
  }

  findEditorialByID(item: any) {
    this.isEdit = true;
    this.item = item;
    $('#modal-add-edit').modal('show');
  }

  async eliminaEditorial(item: any) {
    await Swal.fire({
      title: '¿Esta seguro?',
      text: 'De eliminar el Editorial',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.editorialService.deleteEditorial(item.id).subscribe({
          next: (res: any) => {
            console.log(res);
            if (res.eliminado) {
              Swal.fire('Eliminado!', 'El Editorial se ha sido eliminado.', 'success');
              this.pagination = 1;
              this.listEditorial();
            } else {
              Swal.fire(
                'Eliminado!',
                'No se pudo Eliminar el registro del Editorial.',
                'error'
              );
            }
          },
        });
      }
    });
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.listEditorial();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.listEditorial();
  }

  formAddEditorial(titulo: boolean) {
    this.isEdit = false;
    $('#modal-add-edit').modal('show');
  }

  limpiar(e: boolean) {
    this.item = '';
  }
}
