import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { EditorialService } from 'src/app/services/editorial/editorial.service';
declare var $: any;

@Component({
  selector: 'app-editorial-form',
  templateUrl: './editorial-form.component.html',
  styleUrls: ['./editorial-form.component.css'],
})
export class EditorialFormComponent {
  @Input() tituloForm!: string;
  @Input() isEdit!: boolean;
  @Input() item: any;
  @Output() clearItem = new EventEmitter<boolean>();
  isClicked: boolean = false;
  router: any;
  autores: any;

  constructor(
    public editorialService: EditorialService,
    private alert: AlertsService
  ) {}

  ngOnInit() {}

  ngOnChanges() {
    this.recoveryEditorial();
  }

  get editorial() {
    return this.formEditorial.get('editorial') as FormControl;
  }

  get direccion() {
    return this.formEditorial.get('direccion') as FormControl;
  }

  get pais() {
    return this.formEditorial.get('pais') as FormControl;
  }

  get telefono() {
    return this.formEditorial.get('telefono') as FormControl;
  }

  get email() {
    return this.formEditorial.get('email') as FormControl;
  }

  formEditorial = new FormGroup({
    editorial: new FormControl('', Validators.required),
    direccion: new FormControl('', Validators.required),
    telefono: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    pais: new FormControl('', Validators.required),
  });

  createEditorial() {
    this.isClicked = true;
    if (this.formEditorial.valid) {
      if (this.isEdit) {
        this.editorialService
          .putUpdateEditorial(this.item.id, this.formEditorial.value)
          .subscribe({
            next: (val: any) => {
              if (val.data) {
                this.alert.sweet(
                  'Edición Editorial',
                  'Se realizó correctamente.',
                  'success'
                );
                this.limpiar();
                this.editorialService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              } else {
                this.alert.sweet(
                  'Edición Editorial',
                  'No se pudo realizar la edición.',
                  'error'
                );
                this.limpiar();
                this.editorialService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              }
            },
          });
      } else {
        this.editorialService
          .postAddEditorial(this.formEditorial.value)
          .subscribe({
            next: (val: any) => {
              if (val.estado) {
                this.alert.sweet(
                  'Registro Autor',
                  'Se realizó correctamente.',
                  'success'
                );
                this.limpiar();
                this.editorialService.refresh = true;
              } else {
                this.alert.sweet(
                  'Registro de Editorial',
                  'No se pudo realizar el registro. ',
                  'error'
                );
                this.limpiar();
                this.editorialService.refresh = true;
              }
            },
          });
        this.isClicked = false;
        $('#modal-add-edit').modal('hide');
      }
    }
  }

  recoveryEditorial() {
    if (this.item) {
      this.editorialService.getByIDEditorial(this.item.id).subscribe({
        next: (val: any) => {
          this.formEditorial.patchValue(val.data);
        },
      });
    }
  }

  limpiar() {
    this.formEditorial.reset();
    this.item = '';
    this.isClicked = false;
    this.clearItem.emit(true);
  }
}
