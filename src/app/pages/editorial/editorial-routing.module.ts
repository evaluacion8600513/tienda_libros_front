import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { EditorialComponent } from './editorial.component';
import { EditorialListComponent } from './editorial-list/editorial-list.component';

const routes: Routes = [
  {
    path: '',
    component: EditorialComponent,
    children: [
      { path: '', redirectTo: 'editorial-list', pathMatch: 'full' },
      { path: 'editorial-list', component: EditorialListComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditorialRoutingModule {}
