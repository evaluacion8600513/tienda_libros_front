import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
declare var $: any;

@Component({
  selector: 'app-categoria-form',
  templateUrl: './categoria-form.component.html',
  styleUrls: ['./categoria-form.component.css']
})
export class CategoriaFormComponent {
  @Input() tituloForm!: string;
  @Input() isEdit!: boolean;
  @Input() item: any;
  @Output() clearItem = new EventEmitter<boolean>();
  isClicked: boolean = false;
  router: any;
  autores: any;

  constructor(
    public categoriaService: CategoriaService,
    private alert: AlertsService
  ) {}

  ngOnInit() {}

  ngOnChanges() {
    this.recoveryCategoria();
  }

  get categoria() {
    return this.formCategoria.get('categoria') as FormControl;
  }

  formCategoria = new FormGroup({
    categoria: new FormControl('', Validators.required),
  });

  createCategoria() {
    this.isClicked = true;
    if (this.formCategoria.valid) {
      if (this.isEdit) {
        this.categoriaService
          .putUpdateCategoria(this.item.id, this.formCategoria.value)
          .subscribe({
            next: (val: any) => {
              if (val.data) {
                this.alert.sweet(
                  'Edición Categoria',
                  'Se realizó correctamente.',
                  'success'
                );
                this.limpiar();
                this.categoriaService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              } else {
                this.alert.sweet(
                  'Edición Categoria',
                  'No se pudo realizar la edición.',
                  'error'
                );
                this.limpiar();
                this.categoriaService.refresh = true;
                this.isClicked = false;
                $('#modal-add-edit').modal('hide');
              }
            },
          });
      } else {
        this.categoriaService.postAddCategoria(this.formCategoria.value).subscribe({
          next: (val: any) => {
            if (val.estado) {
              this.alert.sweet(
                'Registro Categoria',
                'Se realizó correctamente.',
                'success'
              );
              this.limpiar();
              this.categoriaService.refresh = true;
            } else {
              this.alert.sweet(
                'Registro de Categoria',
                'No se pudo realizar el registro. ',
                'error'
              );
              this.limpiar();
              this.categoriaService.refresh = true;
            }
          },
        });
        this.isClicked = false;
        $('#modal-add-edit').modal('hide');
      }
    }
  }

  recoveryCategoria() {
    if (this.item) {
      this.categoriaService.getByIDCategoria(this.item.id).subscribe({
        next: (val: any) => {
          this.formCategoria.patchValue(val.data);
        },
      });
    }
  }

  limpiar() {
    this.formCategoria.reset();
    this.item = '';
    this.isClicked = false;
    this.clearItem.emit(true);
  }

}
