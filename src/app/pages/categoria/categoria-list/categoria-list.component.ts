import { Component } from '@angular/core';
import { AlertsService } from 'src/app/helpers/alerts/alerts.service';
import { CategoriaService } from 'src/app/services/categoria/categoria.service';
import Swal from 'sweetalert2';
declare var $: any;

@Component({
  selector: 'app-categoria-list',
  templateUrl: './categoria-list.component.html',
  styleUrls: ['./categoria-list.component.css']
})
export class CategoriaListComponent {
  buscar = { categoria: '' };
  cargando = false;
  categoria!: any[];
  pagination: number = 1;
  cantReg: number = 10;
  allCategoria: number = 0;
  public hasError: boolean = false;

  tituloForm = '';
  isEdit: boolean = false;
  item: any;

  constructor(public categoriaService: CategoriaService, public alert: AlertsService) {}

  ngOnInit() {
    this.listCategoria();
  }

  ngDoCheck() {
    if (this.categoriaService.refresh) {
      this.numRegistro();
      this.categoriaService.refresh = false;
    }
  }

  listCategoria() {
    this.categoriaService.getCategoria(this.pagination, this.cantReg).subscribe({
      next: (res: any) => {
        this.categoria = res.data;
        this.allCategoria = res.total;
        this.cargando = true;
      },
    });
  }

  findCategoria() {
    this.cargando = false;
    if (this.buscar.categoria) {
      this.categoriaService
        .getLikeCategoria(this.pagination, this.cantReg, this.buscar.categoria)
        .subscribe({
          next: (res: any) => {
            this.categoria = res.data;
            this.allCategoria = res.total;
            this.pagination = res.current_page;
            this.cargando = true;
          },
        });
    } else {
      this.listCategoria();
    }
  }

  findCategoriaByID(item: any) {
    this.isEdit = true;
    this.item = item;
    $('#modal-add-edit').modal('show');
  }

  async eliminaCategoria(item: any) {
    await Swal.fire({
      title: '¿Esta seguro?',
      text: 'De eliminar el Autor',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, Eliminar',
    }).then((result) => {
      if (result.isConfirmed) {
        this.categoriaService.deleteCategoria(item.id).subscribe({
          next: (res: any) => {
            console.log(res);
            if (res.eliminado) {
              Swal.fire('Eliminado!', 'La Categoria ha sido eliminado.', 'success');
              this.pagination = 1;
              this.listCategoria();
            } else {
              Swal.fire(
                'Eliminado!',
                'No se pudo Eliminar el registro de Categoria.',
                'error'
              );
            }
          },
        });
      }
    });
  }

  numRegistro() {
    this.cargando = false;
    this.pagination = 1;
    this.listCategoria();
  }

  renderPage(event: number) {
    this.cargando = false;
    this.pagination = event;
    this.listCategoria();
  }

  formAddCategoria(titulo: boolean) {
    this.isEdit = false;
    $('#modal-add-edit').modal('show');
  }

  limpiar(e: boolean) {
    this.item = '';
  }

}
