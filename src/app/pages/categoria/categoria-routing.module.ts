import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CategoriaComponent } from './categoria.component';
import { CategoriaListComponent } from './categoria-list/categoria-list.component';

const routes: Routes = [
  {
    path: '',
    component: CategoriaComponent,
    children: [
      { path: '', redirectTo: 'categoria-list', pathMatch: 'full' },
      { path: 'categoria-list', component: CategoriaListComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CategoriaRoutingModule {}
