export interface CategoriaInterface {
  categoria: string;
  createdAt?: Date;
  updatedAt?: Date;
}
