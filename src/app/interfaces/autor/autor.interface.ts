export interface AutorInterface {
  nombre: string;
  paterno: string;
  materno: string;
  sexo: string;
  direccion: string;
  telefono: string;
  email: string;
  createdAt?: Date;
  updatedAt?: Date;
}
