export interface LibroInterface {
  titulo: string
  num_edicion: string
  num_paginas: string
  id_categorias: string
  fecha_publicacion: string
  sinopsis: string
  img: string
  cantidad: string
  id_editoriales: string
  autores: string
  createdAt?: Date;
  updatedAt?: Date;
}
