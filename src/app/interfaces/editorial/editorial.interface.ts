export interface EditorialInterface {
  editorial: string;
  direccion: string;
  telefono: string;
  email: string;
  pais: string;
  createdAt?: Date;
  updatedAt?: Date;
}
