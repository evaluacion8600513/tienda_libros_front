import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CategoriaInterface } from 'src/app/interfaces/categoria/categoria.interface';
import { Env } from 'src/environments/env';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  private readonly baseUrl: string = Env.baseUrl;
  public refresh!: boolean;
  public data!: CategoriaInterface;

  constructor(private http: HttpClient) {}

  getCategoria(page: number, cant: number): Observable<CategoriaInterface[]> {
    return this.http.get<CategoriaInterface[]>(
      this.baseUrl + 'listCategoria?page=' + page + '&cant=' + cant
    );
  }

  getLikeCategoria(
    page: number,
    cant: number,
    texto: string
  ): Observable<CategoriaInterface[]> {
    return this.http.get<CategoriaInterface[]>(
      this.baseUrl +
        'findByLikeCategoria?page=' +
        page +
        '&cant=' +
        cant +
        '&texto=' +
        texto
    );
  }

  getComboCategoria(): Observable<CategoriaInterface[]> {
    return this.http.get<CategoriaInterface[]>(this.baseUrl + 'allCategoria');
  }

  getByIDCategoria(id: number): Observable<CategoriaInterface> {
    return this.http.get<CategoriaInterface>(this.baseUrl + 'editCategoria/' + id);
  }

  postAddCategoria(data: any): Observable<any> {
    return this.http.post(this.baseUrl + 'registerCategoria', data);
  }

  putUpdateCategoria(id: number, data: any): Observable<any> {
    return this.http.put(this.baseUrl + 'updateCategoria/' + id, data);
  }

  deleteCategoria(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + 'destroyCategoria/' + id);
  }
}
