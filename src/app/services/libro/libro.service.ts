import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LibroInterface } from 'src/app/interfaces/libro/libro.interface';
import { Env } from 'src/environments/env';

@Injectable({
  providedIn: 'root',
})
export class LibroService {
  private readonly baseUrl: string = Env.baseUrl;
  public refresh!: boolean;
  public data!: LibroInterface;

  constructor(private http: HttpClient) {}

  getLibro(page: number, cant: number): Observable<LibroInterface[]> {
    return this.http.get<LibroInterface[]>(
      this.baseUrl + 'listLibros?page=' + page + '&cant=' + cant
    );
  }

  getLikeLibro(
    page: number,
    cant: number,
    autor: string,
    publicacion: string,
    editorial: string,
    categoria: string
  ): Observable<LibroInterface[]> {
    return this.http.get<LibroInterface[]>(
      this.baseUrl +
        'findByLikeLibro?page=' +
        page +
        '&cant=' +
        cant +
        '&autores=' +
        autor +
        '&publicacion=' +
        publicacion +
        '&editorial=' +
        editorial +
        '&categoria=' +
        categoria
    );
  }

  getComboLibro(): Observable<LibroInterface[]> {
    return this.http.get<LibroInterface[]>(this.baseUrl + 'allLibro');
  }

  getByIDLibro(id: number): Observable<LibroInterface> {
    return this.http.get<LibroInterface>(this.baseUrl + 'editLibros/' + id);
  }

  postAddLibro(data: any): Observable<any> {
    return this.http.post(this.baseUrl + 'registerLibros', data);
  }

  putUpdateLibro(
    idLibro: number,
    idLibroAutores: number,
    data: any
  ): Observable<any> {
    return this.http.put(
      this.baseUrl + 'updateLibros/' + idLibro + '/' + idLibroAutores,
      data
    );
  }

  deleteLibro(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + 'destroyLibros/' + id);
  }

  descargarArchivoExcel(): void {
    // Realizar la solicitud GET a la API
    this.http
      .get(this.baseUrl + 'exportar-excel', { responseType: 'arraybuffer' })
      .subscribe(
        (response: ArrayBuffer) => {
          // Crear un objeto Blob para el contenido del archivo
          const blob = new Blob([response], {
            type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
          });

          // Crear un enlace para descargar el archivo
          const url = window.URL.createObjectURL(blob);
          const link = document.createElement('a');
          link.href = url;
          link.download = 'miarchivo.xlsx';
          link.click();

          // Liberar el objeto URL
          window.URL.revokeObjectURL(url);
        },
        (error) => {
          console.error('Error al descargar el archivo Excel', error);
        }
      );
  }
}
