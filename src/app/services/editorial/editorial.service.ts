import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EditorialInterface } from 'src/app/interfaces/editorial/editorial.interface';
import { Env } from 'src/environments/env';

@Injectable({
  providedIn: 'root'
})
export class EditorialService {
  private readonly baseUrl: string = Env.baseUrl;
  public refresh!: boolean;
  public data!: EditorialInterface;

  constructor(private http: HttpClient) { }

  getEditorial(page: number, cant: number): Observable<EditorialInterface[]> {
    return this.http.get<EditorialInterface[]>(
      this.baseUrl + 'listEditorial?page=' + page + '&cant=' + cant
    );
  }

  getLikeEditorial(
    page: number,
    cant: number,
    texto: string
  ): Observable<EditorialInterface[]> {
    return this.http.get<EditorialInterface[]>(
      this.baseUrl +
        'findByLikeEditorial?page=' +
        page +
        '&cant=' +
        cant +
        '&texto=' +
        texto
    );
  }

  getComboEditorial(): Observable<EditorialInterface[]> {
    return this.http.get<EditorialInterface[]>(this.baseUrl + 'allEditorial');
  }

  getByIDEditorial(id: number): Observable<EditorialInterface> {
    return this.http.get<EditorialInterface>(this.baseUrl + 'editEditorial/' + id);
  }

  postAddEditorial(data: any): Observable<any> {
    return this.http.post(this.baseUrl + 'registerEditorial', data);
  }

  putUpdateEditorial(id: number, data: any): Observable<any> {
    return this.http.put(this.baseUrl + 'updateEditorial/' + id, data);
  }

  deleteEditorial(id: number): Observable<any> {
    return this.http.delete(this.baseUrl + 'destroyEditorial/' + id);
  }
}
